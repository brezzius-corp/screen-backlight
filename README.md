# SBacklight

Adjust the brightness of your screen.

## Requirements

In order to build sbacklight you need C89 library and header files.

## Installation

Edit config.mk to match your local setup (sbacklight is installed into the /usr/local namespace by default).

Enter the following commands to download and build sbacklight :

```
git clone https://gitlab.com/brezzius-corp/screen-backlight.git
cd sbacklight
sudo make install
```

## Running

Enter the following command to run sbacklight (if necessary as root) :

`sbacklight [OPTIONS]`

## Options

* `-g, --get, .` Get the current backlight
* `-i, --increase, +` Increase 1 the backlight (max MAX_BRIGHTNESS)
* `-d, --decrease, -` Decrease 1 the backlight (min MIN_BRIGHTNESS)
* `-s, --set` Set the backlight (integer bewtween range [MIN_BRIGHTNESS, MAX_BRIGHTNESS])
* `-h, --help` Show help
* `-v, --version` Show version

## Configuration

Edit config.h to match your backlight configuration files and re(compiling) the source code.

## Uninstall

Enter the following command to uninstall sbacklight :

`sudo make uninstall`
