#ifndef __CONFIG_H
#define __CONFIG_H

#define FILENAME_BRIGHTNESS "/sys/class/backlight/intel_backlight/brightness"
#define FILENAME_MAXBRIGHTNESS "/sys/class/backlight/intel_backlight/max_brightness"
#define DEFAULT_MAXBRIGHTNESS 1500
#define DEFAULT_MINBRIGHTNESS 100
#define DEFAULT_RANGE 7

#endif /* __CONFIG_H */

